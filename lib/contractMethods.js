import "@ethersproject/shims";
import { BigNumber } from "ethers";
import { getContractObj } from "./contract";

export async function mint(_mintAmount, provider) {
  console.log("Normal Minting");
  const myContract = getContractObj(provider);

  try {
    var cost;
    const c = await myContract.cost();
    cost = BigNumber.from(c);
    console.log("cost ==>", cost)

    var tx = await myContract.mint(_mintAmount, {
      value: cost.mul(_mintAmount),
    });
    console.log("tx===>", tx)
    return tx;
  } catch (error) {
    console.log("error==>", error);
    return false;
  }
}

export async function whitelistMint(_mintAmount, provider) {
  console.log("Per-Sale Minting");
  const myContract = getContractObj(provider);

  try {
    var cost;
    const c = await myContract.whitelistCost();
    cost = BigNumber.from(c);
    console.log("cost ==>", cost);

    var tx = await myContract.mintWhitelist(_mintAmount, {
      value: cost.mul(_mintAmount),
    });
    console.log("tx===>", tx)
    return tx;
  } catch (error) {
    console.log("error==>", error);
    return false;
  }
}


export async function getAllInfo(provider) {
  const mycontract = getContractObj(provider);
  try {
    const [
      totalSupply,
      maxSupply,
      mintPrice,
      ownerAddress,
      maxMintAmount,
      paused,
      onlyWhitelisted,
      nftPerAddressLimit,
      remainingPublicSupply,
      remainingWhitelistSupply,
      totalPublicSupply,
      totalWhitelistSupply,
      maxWhitelistSupply

    ] = await Promise.all([
      mycontract.totalSupply(),
      mycontract.maxSupply(),
      mycontract.cost(),
      mycontract.owner(),
      mycontract.maxMintAmount(),
      mycontract.paused(),
      mycontract.onlyWhitelisted(),
      mycontract.nftPerAddressLimit(),
      mycontract.remainingPublicSupply(),
      mycontract.remainingWhitelistSupply(),
      mycontract.totalPublicSupply(),
      mycontract.totalWhitelistSupply(),
      mycontract.maxWhitelistSupply()
    ]);

    return {
      totalSupply,
      maxSupply,
      mintPrice,
      ownerAddress,
      maxMintAmount,
      paused,
      onlyWhitelisted,
      nftPerAddressLimit,
      remainingPublicSupply,
      remainingWhitelistSupply,
      totalPublicSupply,
      totalWhitelistSupply,
      maxWhitelistSupply,

    };
  } catch (error) {
    console.log(error);
  }
}
