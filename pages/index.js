import { useWallet } from '../lib/hooks/wallet';
import Mint from '../components/Mint';
import MintText from '../components/MintForm';

export default function Home() {

  const { active } = useWallet();

  return (
    <>
      {active ? (

        <Mint />

      ) : (

        <MintText />
      )}
    </>
  )
}
